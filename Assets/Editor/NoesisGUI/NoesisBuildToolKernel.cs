using UnityEditor;
using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Reflection;

namespace Noesis
{
    ////////////////////////////////////////////////////////////////////////////////////////////////
    public partial class BuildToolKernel: IDisposable
    {
        private Library library_ = null;
        private static string platform_ = null;

        private delegate void LogCallback(int severity, string message);
        private delegate void RegisterLogCallbackDelegate(LogCallback callback);
        private RegisterLogCallbackDelegate registerLogCallback_ = null;

        private delegate void InitKernelDelegate(string platform, string assetsPath, string streamingAssetsPath);
        private InitKernelDelegate initKernel_ = null;

        private delegate void ShutdownKernelDelegate();
        private ShutdownKernelDelegate shutdownKernel_ = null;

        private delegate void ScanDelegate();
        private ScanDelegate scan_ = null;

        private delegate void BuildDelegate();
        private BuildDelegate build_ = null;

        private delegate void RebuildDelegate();
        private BuildDelegate rebuild_ = null;

        private delegate void CleanDelegate();
        private CleanDelegate clean_ = null;

        public BuildToolKernel(string platform)
        {
            library_ = new Library(UnityEngine.Application.dataPath + "/Editor/NoesisGUI/BuildTool/Noesis");
            platform_ = platform;

            try
            {
                RegisterFunctions(library_);
                Error.RegisterFunctions(library_);
                Log.RegisterFunctions(library_);
                _Extend.RegisterFunctions(library_);
                _NoesisGUI_PINVOKE.RegisterFunctions(library_);

                registerLogCallback_(OnLog);
                _Extend.RegisterCallbacks();

                initKernel_(platform, UnityEngine.Application.dataPath, UnityEngine.Application.streamingAssetsPath);
                Error.Check();

                Log.Info(String.Format("Host is Unity v{0}", UnityEngine.Application.unityVersion));

                _Extend.RegisterExtendClasses();
            }
            catch(Exception e)
            {
                Dispose();
                throw e;
            }
        }

        ~BuildToolKernel()
        {
            Dispose();
        }

        public void Dispose()
        {
            if (shutdownKernel_ != null)
            {
                System.GC.Collect();
                System.GC.WaitForPendingFinalizers();
                shutdownKernel_();

                _Extend.UnregisterCallbacks();

                UnregisterFunctions();
                Error.UnregisterFunctions();
                Log.UnregisterFunctions();
                _Extend.UnregisterFunctions();
                _NoesisGUI_PINVOKE.UnregisterFunctions();

                library_.Dispose();
            }

            System.GC.SuppressFinalize(this);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private void RefreshComponents()
        {
            // This could be improved a lot, because SetDirty notifies the inspector of a change but
            // also marks the component to be serialized to disk. We could use a delegate to notity our
            // custom editor.
            // Although for now it doesn't seem to be necessary
            UnityEngine.Object[] objs = UnityEngine.Object.FindObjectsOfType(typeof(NoesisGUIPanel));
            foreach (UnityEngine.Object obj in objs)
            {
                NoesisGUIPanel noesisGUI = (NoesisGUIPanel)obj;
                EditorUtility.SetDirty(noesisGUI);
                
            }

            PlayerPrefs.Save();
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        public void Scan()
        {
            Log.Info(String.Format("SCAN {0}", platform_));
            scan_();
            Error.Check();
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        public void Clean()
        {
            Log.Info(String.Format("CLEAN {0}", platform_));
            clean_();
            Error.Check();
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        public void Build()
        {
            Log.Info(String.Format("BUILD {0}", platform_));
            build_();
            Error.Check();
            RefreshComponents();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public void Rebuild()
        {
            Log.Info(String.Format("REBUILD {0}", platform_));
            rebuild_();
            Error.Check();
            RefreshComponents();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private void RegisterFunctions(Noesis.Library lib)
        {
            registerLogCallback_ = lib.Find<RegisterLogCallbackDelegate>("Noesis_RegisterLogCallback");
            initKernel_ = lib.Find<InitKernelDelegate>("Noesis_InitBuildTool");
            shutdownKernel_ = lib.Find<ShutdownKernelDelegate>("Noesis_ShutdownBuildTool");
            scan_ = lib.Find<ScanDelegate>("Noesis_Scan");
            build_ = lib.Find<BuildDelegate>("Noesis_Build");
            rebuild_ = lib.Find<BuildDelegate>("Noesis_Rebuild");
            clean_ = lib.Find<CleanDelegate>("Noesis_Clean");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private void UnregisterFunctions()
        {
            registerLogCallback_ = null;
            initKernel_ = null;
            shutdownKernel_ = null;
            scan_ = null;
            build_ = null;
            rebuild_ = null;
            clean_ = null;
        } 

        ////////////////////////////////////////////////////////////////////////////////////////////////
        private static string assetBeingProcessed_;

        ////////////////////////////////////////////////////////////////////////////////////////////////
        [MonoPInvokeCallback (typeof(LogCallback))]
        private static void OnLog(int severity, string message)
        {
            message = message.Replace("Unity/Unity/", "Assets/");

            switch (severity)
            {
                case 0: // Critical
                { 
                    Debug.LogError(String.Format("[{0}] {1}\n{2}", platform_, assetBeingProcessed_, message));
                    PlayerPrefs.SetString(assetBeingProcessed_ + "_error", message);
                    break;
                }
                case 10: // Warning
                {
                    Debug.LogWarning(String.Format("[{0}] {1}\n{2}", platform_, assetBeingProcessed_, message));
                    PlayerPrefs.SetString(assetBeingProcessed_ + "_warning", message);
                    break;
                }
                case 20: // Info
                {
                    if (message.StartsWith("> Building "))
                    {
                        assetBeingProcessed_ = message.Substring(11);
                        PlayerPrefs.DeleteKey(assetBeingProcessed_ + "_warning");
                        PlayerPrefs.DeleteKey(assetBeingProcessed_ + "_error");
                    }
                    break;
                }
                case 30: // Debug
                {
                    break;
                }
            }
        }
    }
}
