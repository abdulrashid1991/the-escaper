using UnityEngine;
using System;
using System.Runtime.InteropServices;

namespace Noesis
{
    public enum AntialiasingMode
    {
        MSAA,
        PPAA
    }

    public enum TessellationMode
    {
        Once,
        Always,
        Threshold
    }

    public enum TessellationQuality
    {
        Low,
        Medium,
        High
    }

    public enum RendererFlags
    {
        None = 0,
        Wireframe = 1,
        ColorBatches = 2,
        ShowOverdraw = 4,
        FlipY = 8
    }

    /////////////////////////////////////////////////////////////////////////////////////
    /// Manages updates, render and input events of a Noesis UI panel
    /////////////////////////////////////////////////////////////////////////////////////
    internal partial class UIRenderer
    {
        /////////////////////////////////////////////////////////////////////////////////
        private int _rendererId;
        private FrameworkElement _root;
        private Vector2 _offscreenSize;
        private Vector3 _mousePos;
        
        private GameObject _target;
        private RenderTexture _texture;
        
        /////////////////////////////////////////////////////////////////////////////////
        public UIRenderer(FrameworkElement root, Vector2 offscreenSize, GameObject target)
        {
            _root = root;
            _offscreenSize = offscreenSize;
            _rendererId = Noesis_CreateRenderer(FrameworkElement.getCPtr(_root).Handle);
            _mousePos = Input.mousePosition;
            _target = target;
            _texture = FindTexture();

#if UNITY_4_3
            // Note that this function works with a null object (meaning the backbuffer)
            if (!RenderTexture.SupportsStencil(_texture))
            {
                Debug.LogWarning("Masking disabled because Stencil Buffer is not present" 
                    + ". Use a 24-bits DepthBuffer to activate it.");
            }
#endif
        }

        /////////////////////////////////////////////////////////////////////////////////
        public void Destroy()
        {
            _root = null;
            Noesis_NotifyDestroyRenderer(_rendererId);
        }

        /////////////////////////////////////////////////////////////////////////////////
        public void Update(double timeInSeconds, AntialiasingMode aaMode,
            TessellationMode tessMode, TessellationQuality tessQuality,
            RendererFlags flags, bool enableMouse, bool enableTouch)
        {
            UpdateSettings(aaMode, tessMode, tessQuality, flags);
            UpdateInputs(enableMouse, enableTouch);
            Noesis_UpdateRenderer(_rendererId, timeInSeconds);
        }

        /////////////////////////////////////////////////////////////////////////////////
        enum RenderEvent
        {
            // custom IDs
            PreRender = 2000,
            PostRender = 1000
        };

        /////////////////////////////////////////////////////////////////////////////////
        public void PreRender()
        {
            IssueRenderEvent(RenderEvent.PreRender);

            // For a weird reason, if we invalidate state here in Windows OpenGL, Unity scene 
            // is not rendered any more. Instead is working without this invalidate
            if (!_isWindowsGL)
            {
                GL.InvalidateState();
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        public void PostRender()
        {
            IssueRenderEvent(RenderEvent.PostRender);
            GL.InvalidateState();
        }

        /////////////////////////////////////////////////////////////////////////////////
        private int _lastFrameCount = -1;
        public void RenderToTexture()
        {
            if (_texture != null)
            {
                bool textureRendered = _lastFrameCount == Time.frameCount;
                if (!textureRendered)
                {
                    _lastFrameCount = Time.frameCount;

                    IssueRenderEvent(RenderEvent.PreRender);

                    RenderTexture prev = RenderTexture.active;
                    RenderTexture.active = _texture;
                    IssueRenderEvent(RenderEvent.PostRender);

#if UNITY_4_3
                    _texture.DiscardContents(false, true);
#endif

                    GL.InvalidateState();
                    RenderTexture.active = prev;
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        public T GetRoot<T>() where T : BaseComponent
        {
            return _root.As<T>();
        }

        /////////////////////////////////////////////////////////////////////////////////
        private bool _shiftPressed = false;
        public void ProcessEvent(UnityEngine.Event ev, bool enableKeyboard, bool enableMouse)
        {
#if !UNITY_STANDALONE && !UNITY_EDITOR
            enableKeyboard = false;
            enableMouse = false;
#endif

            // Shift key is not correctly notified via ev.type and ev.keyCode
            if (enableKeyboard)
            {
                if (ev.shift)
                {
                    if (!_shiftPressed)
                    {
                        _shiftPressed = true;
                        Noesis_KeyDown(_rendererId, (int)Key.Shift);
                    }
                }
                else
                {
                    if (_shiftPressed)
                    {
                        _shiftPressed = false;
                        Noesis_KeyUp(_rendererId, (int)Key.Shift);
                    }
                }
            }

            switch (ev.type)
            {
                case EventType.MouseDown:
                {
                    if (enableMouse)
                    {
                        Vector2 mouse = ProjectPointer(ev.mousePosition.x, Screen.height - ev.mousePosition.y);

                        if (HitTest(mouse.x, mouse.y))
                        {
                            ev.Use();
                        }

                        Noesis_MouseButtonDown(_rendererId, mouse.x, mouse.y, ev.button);

                        if (ev.clickCount == 2)
                        {
                            Noesis_MouseDoubleClick(_rendererId, mouse.x, mouse.y, ev.button);
                        }
                    }
                    break;
                }
                case EventType.MouseUp:
                {
                    if (enableMouse)
                    {
                        Vector2 mouse = ProjectPointer(ev.mousePosition.x, Screen.height - ev.mousePosition.y);

                        if (HitTest(mouse.x, mouse.y))
                        {
                            ev.Use();
                        }

                        Noesis_MouseButtonUp(_rendererId, mouse.x, mouse.y, ev.button);
                    }
                    break;
                }
                case EventType.KeyDown:
                {
                    if (enableKeyboard)
                    {
                        if (ev.keyCode != KeyCode.None)
                        {
                            int noesisKeyCode = NoesisKeyCodes.Convert(ev.keyCode);
                            if (noesisKeyCode != 0)
                            {
                                Noesis_KeyDown(_rendererId, noesisKeyCode);
                            }
                        }
                        else if (ev.character != 0)
                        {
                            Noesis_Char(_rendererId, ev.character);
                        }
                    }
                    break;
                }
                case EventType.KeyUp:
                {
                    if (enableKeyboard)
                    {
                        if (ev.keyCode != KeyCode.None)
                        {
                            int noesisKeyCode = NoesisKeyCodes.Convert(ev.keyCode);
                            if (noesisKeyCode != 0)
                            {
                                Noesis_KeyUp(_rendererId, noesisKeyCode);
                            }
                        }
                    }
                    break;
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        public void Activate()
        {
            Noesis_Activate(_rendererId);
        }

        /////////////////////////////////////////////////////////////////////////////////
        public void Deactivate()
        {
            Noesis_Deactivate(_rendererId);
        }

        /////////////////////////////////////////////////////////////////////////////////
        private void UpdateSettings(AntialiasingMode aaMode, TessellationMode tessMode,
            TessellationQuality tessQuality, RendererFlags flags)
        {
            // update renderer size
            if (_texture == null)
            {
                Noesis_RendererSurfaceSize(_rendererId, Screen.width, Screen.height,
                    _offscreenSize.x, _offscreenSize.y, QualitySettings.antiAliasing);

                if (_isGraphicsDeviceDirectX)
                {
                    Camera camera = _target.camera != null ? _target.camera : Camera.main;
                    if (camera != null &&
                        camera.actualRenderingPath == RenderingPath.DeferredLighting)
                    {
                        flags |= RendererFlags.FlipY;
                    }
                }
            }
            else // Render to Texture
            {
                System.Diagnostics.Debug.Assert(_texture.width > 0);
                System.Diagnostics.Debug.Assert(_texture.height > 0);

                Noesis_RendererSurfaceSize(_rendererId, _texture.width, _texture.height,
                    _offscreenSize.x, _offscreenSize.y, QualitySettings.antiAliasing);

                if (_isGraphicsDeviceDirectX)
                {
                    flags |= RendererFlags.FlipY;
                }
            }

            // update renderer settings
            Noesis_RendererAntialiasingMode(_rendererId, (int)aaMode);
            Noesis_RendererTessMode(_rendererId, (int)tessMode);
            Noesis_RendererTessQuality(_rendererId, (int)tessQuality);
            Noesis_RendererFlags(_rendererId, (int)flags);
        }

        /////////////////////////////////////////////////////////////////////////////////
        private void UpdateInputs(bool enableMouse, bool enableTouch)
        {
#if !UNITY_STANDALONE && !UNITY_EDITOR
            enableMouse = false;
#endif
            if (enableMouse)
            {
                // mouse move
                if (_mousePos != Input.mousePosition)
                {
                    _mousePos = Input.mousePosition;
                    Vector2 mouse = ProjectPointer(_mousePos.x, _mousePos.y);
                    Noesis_MouseMove(_rendererId, mouse.x, mouse.y);
                }

                // mouse wheel
                int mouseWheel = (int)(Input.GetAxis("Mouse ScrollWheel") * 10.0f);
                if (mouseWheel != 0)
                {
                    Vector2 mouse = ProjectPointer(_mousePos.x, _mousePos.y);
                    Noesis_MouseWheel(_rendererId, mouse.x, mouse.y, mouseWheel);
                }
            }

            if (enableTouch)
            {
                for (int i = 0; i < Input.touchCount; i++) 
                {
                    Touch touch = Input.GetTouch(i);
                    int id = touch.fingerId;
                    Vector2 pos = ProjectPointer(touch.position.x, touch.position.y);
                    TouchPhase phase = touch.phase;

                    if (phase == TouchPhase.Began)
                    {
                        Noesis_TouchDown(_rendererId, pos.x, pos.y, id);
                    }
                    else if (phase == TouchPhase.Moved || phase == TouchPhase.Stationary)
                    {
                        Noesis_TouchMove(_rendererId, pos.x, pos.y, id);
                    }
                    else
                    {
                        Noesis_TouchUp(_rendererId, pos.x, pos.y, id);
                    }
                }
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        private Vector2 ProjectPointer(float x, float y)
        {
            if (_texture == null)
            {
                // Screen coordinates
                return new Vector2(x, Screen.height - y);
            }
            else
            {
                // Texture coordinates

                // NOTE: A MeshCollider must be attached to the target to obtain valid
                // texture coordintates, otherwise Hit Testing won't work

                Ray ray = Camera.main.ScreenPointToRay(new Vector3(x, y, 0));

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject == _target)
                    {
                        float localX = hit.textureCoord.x * _texture.width;
                        float localY = _texture.height - hit.textureCoord.y * _texture.height;
                        return new Vector2(localX, localY);
                    }
                }

                return new Vector2(-1, -1);
            }
        }
        
        /////////////////////////////////////////////////////////////////////////////////
        private bool HitTest(float x, float y)
        {
            return Noesis_HitTest(FrameworkElement.getCPtr(_root).Handle, x, y);
        }

        /////////////////////////////////////////////////////////////////////////////////
        private RenderTexture FindTexture()
        {
            // Check if NoesisGUI was attached to a GameObject with a RenderTexture set
            // in the diffuse texture of its main Material
            if (_target.renderer != null && _target.renderer.material != null)
            {
                return _target.renderer.material.mainTexture as RenderTexture;
            }
            else
            {
                return null;
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        private void IssueRenderEvent(RenderEvent renderEvent)
        {
            // This triggers the Noesis rendering event
            if (_isMobile)
            {
                // NOTE: We have to make an indirect call to the PInvoke function to avoid
                //  that referenced library gets loaded just by entering current function
                IssuePluginEventMobile((System.Int32)renderEvent + _rendererId);
            }
            else
            {
                GL.IssuePluginEvent((System.Int32)renderEvent + _rendererId);
            }
        }

        /////////////////////////////////////////////////////////////////////////////////
        private void IssuePluginEventMobile(int eventId)
        {
            UnityRenderEvent(eventId);
        }

        /////////////////////////////////////////////////////////////////////////////////
        enum GfxDeviceRenderer
        {
            OpenGL = 0,              // OpenGL
            D3D9 = 1,                // Direct3D 9
            D3D11 = 2,               // Direct3D 11
            GCM = 3,                 // Sony PlayStation 3 GCM
            Null = 4,                // "null" device (used in batch mode)
            Hollywood = 5,           // Nintendo Wii
            Xenon = 6,               // Xbox 360
            OpenGLES = 7,            // OpenGL ES 1.1
            OpenGLES20Mobile = 8,    // OpenGL ES 2.0 mobile variant
            Molehill = 9,            // Flash 11 Stage3D
            OpenGLES20Desktop = 10   // OpenGL ES 2.0 desktop variant (i.e. NaCl)
        };

        private static bool _isGraphicsDeviceDirectX = false;
        private static bool _isWindowsGL = false;
        private static bool _isMobile = false;

        public static void SetDeviceType(int deviceType)
        {
            GfxDeviceRenderer gfxDeviceRenderer = (GfxDeviceRenderer)deviceType;

            _isGraphicsDeviceDirectX = gfxDeviceRenderer == GfxDeviceRenderer.D3D9 ||
                gfxDeviceRenderer == GfxDeviceRenderer.D3D11;

            UnityEngine.RuntimePlatform platform = UnityEngine.Application.platform;

            _isWindowsGL = gfxDeviceRenderer == GfxDeviceRenderer.OpenGL && (
                platform == UnityEngine.RuntimePlatform.WindowsEditor ||
                platform == UnityEngine.RuntimePlatform.WindowsPlayer);

            _isMobile = platform == UnityEngine.RuntimePlatform.IPhonePlayer ||
                platform == UnityEngine.RuntimePlatform.Android;
        }
    }
}
