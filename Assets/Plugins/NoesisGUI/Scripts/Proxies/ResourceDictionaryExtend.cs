using System;
using System.Runtime.InteropServices;

namespace Noesis
{

    public partial class ResourceDictionary
    {
        public T FindName<T>(string name) where T : BaseComponent
        {
            BaseComponent element = FindName(name);
            return (element == null) ? null : element.As<T>();
        }
    }

}