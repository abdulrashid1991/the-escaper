/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class BooleanAnimationUsingKeyFrames : AnimationTimeline {

  internal BooleanAnimationUsingKeyFrames(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.BooleanAnimationUsingKeyFrames_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(BooleanAnimationUsingKeyFrames obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public BooleanAnimationUsingKeyFrames() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(BooleanAnimationUsingKeyFrames)) {
      return NoesisGUI_PINVOKE.new_BooleanAnimationUsingKeyFrames();
    }
    else {
      return base.CreateCPtr(type);
    }
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_BooleanAnimationUsingKeyFrames(valPtr);
  }

}

}

