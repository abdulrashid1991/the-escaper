/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class RectAnimation : BaseAnimation {

  internal RectAnimation(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.RectAnimation_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(RectAnimation obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public RectAnimation() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(RectAnimation)) {
      return NoesisGUI_PINVOKE.new_RectAnimation();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public Rect GetBy() {
    NullableRect ret = new NullableRect(NoesisGUI_PINVOKE.RectAnimation_GetBy(swigCPtr), false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    Rect retNullable = null;
    if (ret.HasValue()) {
      retNullable = ret.GetValue();
    }
    return retNullable;
  }

  public void SetBy(Rect value_) {
    NullableRect value = new NullableRect();
    if (value_ != null) {
      value.SetValue(value_);
    }

    NoesisGUI_PINVOKE.RectAnimation_SetBy(swigCPtr, NullableRect.getCPtr(value));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public NullableRect GetFrom() {
    NullableRect ret = new NullableRect(NoesisGUI_PINVOKE.RectAnimation_GetFrom(swigCPtr), false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetFrom(NullableRect from) {
    NoesisGUI_PINVOKE.RectAnimation_SetFrom(swigCPtr, NullableRect.getCPtr(from));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public NullableRect GetTo() {
    NullableRect ret = new NullableRect(NoesisGUI_PINVOKE.RectAnimation_GetTo(swigCPtr), false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetTo(NullableRect to) {
    NoesisGUI_PINVOKE.RectAnimation_SetTo(swigCPtr, NullableRect.getCPtr(to));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty ByProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.RectAnimation_ByProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty FromProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.RectAnimation_FromProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ToProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.RectAnimation_ToProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_RectAnimation(valPtr);
  }

}

}

