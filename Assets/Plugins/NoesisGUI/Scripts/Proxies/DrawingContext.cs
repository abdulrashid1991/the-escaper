/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class DrawingContext : BaseComponent {

  internal DrawingContext(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.DrawingContext_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(DrawingContext obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public DrawingContext() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(DrawingContext)) {
      return NoesisGUI_PINVOKE.new_DrawingContext();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public void Reset() {
    NoesisGUI_PINVOKE.DrawingContext_Reset(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void DrawEllipse(Brush brush, Pen pen, Point center, float radiusX, float radiusY) {
    NoesisGUI_PINVOKE.DrawingContext_DrawEllipse(swigCPtr, Brush.getCPtr(brush), Pen.getCPtr(pen), Point.getCPtr(center), radiusX, radiusY);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void DrawGeometry(Brush brush, Pen pen, Geometry geometry) {
    NoesisGUI_PINVOKE.DrawingContext_DrawGeometry(swigCPtr, Brush.getCPtr(brush), Pen.getCPtr(pen), Geometry.getCPtr(geometry));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void DrawImage(ImageSource imageSource, Rect rect) {
    NoesisGUI_PINVOKE.DrawingContext_DrawImage(swigCPtr, ImageSource.getCPtr(imageSource), Rect.getCPtr(rect));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void DrawLine(Pen pen, Point p0, Point p1) {
    NoesisGUI_PINVOKE.DrawingContext_DrawLine(swigCPtr, Pen.getCPtr(pen), Point.getCPtr(p0), Point.getCPtr(p1));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void DrawRectangle(Brush brush, Pen pen, Rect rect) {
    NoesisGUI_PINVOKE.DrawingContext_DrawRectangle(swigCPtr, Brush.getCPtr(brush), Pen.getCPtr(pen), Rect.getCPtr(rect));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void DrawRoundedRectangle(Brush brush, Pen pen, Rect rect, float radiusX, float radiusY) {
    NoesisGUI_PINVOKE.DrawingContext_DrawRoundedRectangle(swigCPtr, Brush.getCPtr(brush), Pen.getCPtr(pen), Rect.getCPtr(rect), radiusX, radiusY);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void DrawText(FormattedText formattedText, Rect bounds) {
    NoesisGUI_PINVOKE.DrawingContext_DrawText(swigCPtr, FormattedText.getCPtr(formattedText), Rect.getCPtr(bounds));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void Pop() {
    NoesisGUI_PINVOKE.DrawingContext_Pop(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void PushClip(Geometry clipGeometry) {
    NoesisGUI_PINVOKE.DrawingContext_PushClip(swigCPtr, Geometry.getCPtr(clipGeometry));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void PushOpacity(float opacity) {
    NoesisGUI_PINVOKE.DrawingContext_PushOpacity(swigCPtr, opacity);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void PushOpacityMask(Brush opacityMask) {
    NoesisGUI_PINVOKE.DrawingContext_PushOpacityMask(swigCPtr, Brush.getCPtr(opacityMask));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public void PushTransform(UITransform transform) {
    NoesisGUI_PINVOKE.DrawingContext_PushTransform(swigCPtr, UITransform.getCPtr(transform));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_DrawingContext(valPtr);
  }

}

}

