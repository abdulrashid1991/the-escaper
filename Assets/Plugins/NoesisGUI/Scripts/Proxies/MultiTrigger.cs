/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class MultiTrigger : BaseTrigger {

  internal MultiTrigger(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.MultiTrigger_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(MultiTrigger obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public MultiTrigger() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(MultiTrigger)) {
      return NoesisGUI_PINVOKE.new_MultiTrigger();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public ConditionCollection GetConditions() {
    IntPtr cPtr = NoesisGUI_PINVOKE.MultiTrigger_GetConditions(swigCPtr);
    ConditionCollection ret = (cPtr == IntPtr.Zero) ? null : new ConditionCollection(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public BaseSetterCollection GetSetters() {
    IntPtr cPtr = NoesisGUI_PINVOKE.MultiTrigger_GetSetters(swigCPtr);
    BaseSetterCollection ret = (cPtr == IntPtr.Zero) ? null : new BaseSetterCollection(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public Setter FindSetter(DependencyProperty dp) {
    IntPtr cPtr = NoesisGUI_PINVOKE.MultiTrigger_FindSetter(swigCPtr, DependencyProperty.getCPtr(dp));
    Setter ret = (cPtr == IntPtr.Zero) ? null : new Setter(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public bool HasCondition(DependencyProperty dp) {
    bool ret = NoesisGUI_PINVOKE.MultiTrigger_HasCondition(swigCPtr, DependencyProperty.getCPtr(dp));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public override void RegisterEvents(FrameworkElement target, FrameworkElement nameScope, bool skipSourceName) {
    NoesisGUI_PINVOKE.MultiTrigger_RegisterEvents(swigCPtr, FrameworkElement.getCPtr(target), FrameworkElement.getCPtr(nameScope), skipSourceName);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public override void UnregisterEvents(FrameworkElement target, FrameworkElement nameScope, bool skipSourceName) {
    NoesisGUI_PINVOKE.MultiTrigger_UnregisterEvents(swigCPtr, FrameworkElement.getCPtr(target), FrameworkElement.getCPtr(nameScope), skipSourceName);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public override bool Check(FrameworkElement target, FrameworkElement nameScope, DependencyObject arg2, DependencyProperty dp, bool skipSourceName) {
    bool ret = NoesisGUI_PINVOKE.MultiTrigger_Check(swigCPtr, FrameworkElement.getCPtr(target), FrameworkElement.getCPtr(nameScope), DependencyObject.getCPtr(arg2), DependencyProperty.getCPtr(dp), skipSourceName);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public override BaseComponent FindValue(FrameworkElement target, FrameworkElement nameScope, DependencyObject arg2, DependencyProperty dp) {
    IntPtr cPtr = NoesisGUI_PINVOKE.MultiTrigger_FindValue(swigCPtr, FrameworkElement.getCPtr(target), FrameworkElement.getCPtr(nameScope), DependencyObject.getCPtr(arg2), DependencyProperty.getCPtr(dp));
    BaseComponent ret = (cPtr == IntPtr.Zero) ? null : new BaseComponent(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public override void Invalidate(FrameworkElement target, FrameworkElement nameScope, bool skipSourceName, bool skipTargetName, byte priority) {
    NoesisGUI_PINVOKE.MultiTrigger_Invalidate__SWIG_0(swigCPtr, FrameworkElement.getCPtr(target), FrameworkElement.getCPtr(nameScope), skipSourceName, skipTargetName, priority);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_MultiTrigger(valPtr);
  }

}

}

