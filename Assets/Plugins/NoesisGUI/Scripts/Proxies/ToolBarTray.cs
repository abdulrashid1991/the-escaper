/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class ToolBarTray : FrameworkElement {

  internal ToolBarTray(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.ToolBarTray_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(ToolBarTray obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public ToolBarTray() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(ToolBarTray)) {
      return NoesisGUI_PINVOKE.new_ToolBarTray();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public static bool GetIsLocked(DependencyObject element) {
    bool ret = NoesisGUI_PINVOKE.ToolBarTray_GetIsLocked(DependencyObject.getCPtr(element));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static void SetIsLocked(DependencyObject element, bool isLocked) {
    NoesisGUI_PINVOKE.ToolBarTray_SetIsLocked(DependencyObject.getCPtr(element), isLocked);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Brush GetBackground() {
    IntPtr cPtr = NoesisGUI_PINVOKE.ToolBarTray_GetBackground(swigCPtr);
    Brush ret = (cPtr == IntPtr.Zero) ? null : new Brush(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetBackground(Brush brush) {
    NoesisGUI_PINVOKE.ToolBarTray_SetBackground(swigCPtr, Brush.getCPtr(brush));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Orientation GetOrientation() {
    Orientation ret = (Orientation)NoesisGUI_PINVOKE.ToolBarTray_GetOrientation(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetOrientation(Orientation orientation) {
    NoesisGUI_PINVOKE.ToolBarTray_SetOrientation(swigCPtr, (int)orientation);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty BackgroundProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBarTray_BackgroundProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty IsLockedProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBarTray_IsLockedProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty OrientationProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBarTray_OrientationProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static void Extend(System.Type type) {
    IntPtr typePtr = Noesis.Extend.GetPtrForType(type);
    NoesisGUI_PINVOKE.Extend_ToolBarTray(typePtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_ToolBarTray(valPtr);
  }

}

}

