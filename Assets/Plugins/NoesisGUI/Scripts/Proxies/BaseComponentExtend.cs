using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Reflection;

namespace Noesis
{

    public partial class BaseComponent
    {
        protected BaseComponent()
        {
            System.Type type = this.GetType();
            if (Noesis.Extend.NeedsCreateCPtr(type))
            {
                Init(CreateCPtr(type), true);
            }
            else
            {
                Init(Noesis.Extend.GetCPtr(type), false);
            }
        }

        private void Init(System.IntPtr cPtr, bool cMemoryOwn)
        {
            swigCPtr = new HandleRef(this, cPtr);

            if (cPtr != IntPtr.Zero && !cMemoryOwn)
            {
                NoesisGUI_.BaseComponent_AddReference(this);
            }

            #if UNITY_EDITOR
            if (NoesisGUI_PINVOKE.SWIGPendingException.Pending)
                throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
            #endif
        }

        protected virtual System.IntPtr CreateCPtr(System.Type type)
        {
            System.IntPtr cPtr = Noesis.Extend.New(type);
            Noesis.Extend.Register(type, cPtr, this);
            return cPtr;
        }

        protected void NotifyPropertyChanged(string propertyName)
        {
            Noesis.Extend.PropertyChanged(this.GetType(), swigCPtr.Handle, propertyName);
        }

        public string AsString()
        {
            NsString nsstring = NoesisGUI_.UnboxString(this);
            string ret = nsstring.c_str();
            return ret;
        }

        public T As<T>() where T : BaseComponent
        {
            return (T)BaseComponent.AsType(typeof(T), swigCPtr.Handle, this);
        }

        internal static object AsType(Type type, IntPtr cPtr, BaseComponent obj)
        {
            if (cPtr == IntPtr.Zero)
            {
                return null;
            }

            if (Noesis.Extend.IsExtendType(type))
            {
                IntPtr typeClassPtr = Noesis.Extend.GetPtrForType(type);
                IntPtr handlePtr = Noesis.Extend.TryGetHandle(typeClassPtr, cPtr);

                if (handlePtr != System.IntPtr.Zero)
                {
                    GCHandle gcHandle = GCHandle.FromIntPtr(handlePtr);
                    return gcHandle.Target;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                BaseComponent component = obj != null ? obj : new BaseComponent(cPtr, false);
                MethodInfo checkType = type.GetMethod("CheckType", BindingFlags.NonPublic | BindingFlags.Static);
                if ((bool)checkType.Invoke(null, new object[] { component }))
                {
                    return Activator.CreateInstance(type, BindingFlags.CreateInstance |
                        BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null,
                        new object[] { cPtr, false }, null);
                }

                return null;
            }
        }

        public static bool operator ==(BaseComponent a, BaseComponent b)
        {
            // If both are null, or both are the same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if ((object)a == null || (object)b == null)
            {
                return false;
            }

            // Return true if wrapped c++ objects match:
            return a.swigCPtr.Handle == b.swigCPtr.Handle;
        }

        public static bool operator !=(BaseComponent a, BaseComponent b)
        {
            return !(a == b);
        }

        public override bool Equals(object o)
        {
            return this == o as BaseComponent;
        }

        public override int GetHashCode()
        {
            return swigCPtr.Handle.GetHashCode();
        }
    }

}