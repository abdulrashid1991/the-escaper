/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class LinearDoubleKeyFrame : DoubleKeyFrame {

  internal LinearDoubleKeyFrame(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.LinearDoubleKeyFrame_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(LinearDoubleKeyFrame obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public LinearDoubleKeyFrame() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(LinearDoubleKeyFrame)) {
      return NoesisGUI_PINVOKE.new_LinearDoubleKeyFrame();
    }
    else {
      return base.CreateCPtr(type);
    }
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_LinearDoubleKeyFrame(valPtr);
  }

}

}

