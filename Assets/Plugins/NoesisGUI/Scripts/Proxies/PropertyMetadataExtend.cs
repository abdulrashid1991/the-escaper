using System;
using System.Runtime.InteropServices;

namespace Noesis
{

    public partial class PropertyMetadata
    {
        public PropertyMetadata(object defaultValue) : this(Create(defaultValue), true)
        {
        }

        private static IntPtr Create(object defaultValue)
        {
            if (defaultValue == null)
            {
                return Noesis_CreatePropertyMetadata_BaseComponent(IntPtr.Zero);
            }
            if (defaultValue is bool)
            {
                return Noesis_CreatePropertyMetadata_Bool((bool)defaultValue);
            }
            if (defaultValue is float)
            {
                return Noesis_CreatePropertyMetadata_Float((float)defaultValue);
            }
            if (defaultValue is int)
            {
                return Noesis_CreatePropertyMetadata_Int((int)defaultValue);
            }
            if (defaultValue is uint)
            {
                return Noesis_CreatePropertyMetadata_UInt((uint)defaultValue);
            }
            if (defaultValue is short)
            {
                return Noesis_CreatePropertyMetadata_Short((short)defaultValue);
            }
            if (defaultValue is ushort)
            {
                return Noesis_CreatePropertyMetadata_UShort((ushort)defaultValue);
            }
            if (defaultValue.GetType().IsEnum)
            {
                defaultValue = defaultValue.ToString();
            }
            if (defaultValue is string)
            {
                IntPtr defPtr = Marshal.StringToHGlobalAnsi((string)defaultValue);
                return Noesis_CreatePropertyMetadata_String(defPtr);
            }
            if (defaultValue is Color)
            {
                IntPtr defPtr = Color.getCPtr((Color)defaultValue).Handle;
                return Noesis_CreatePropertyMetadata_Color(defPtr);
            }
            if (defaultValue is Point)
            {
                IntPtr defPtr = Point.getCPtr((Point)defaultValue).Handle;
                return Noesis_CreatePropertyMetadata_Point(defPtr);
            }
            if (defaultValue is Rect)
            {
                IntPtr defPtr = Rect.getCPtr((Rect)defaultValue).Handle;
                return Noesis_CreatePropertyMetadata_Rect(defPtr);
            }
            if (defaultValue is Size)
            {
                IntPtr defPtr = Size.getCPtr((Size)defaultValue).Handle;
                return Noesis_CreatePropertyMetadata_Size(defPtr);
            }
            if (defaultValue is Thickness)
            {
                IntPtr defPtr = Thickness.getCPtr((Thickness)defaultValue).Handle;
                return Noesis_CreatePropertyMetadata_Thickness(defPtr);
            }
            if (defaultValue is Type)
            {
                IntPtr defPtr = Noesis.Extend.GetResourceKeyType(defaultValue as Type);
                return Noesis_CreatePropertyMetadata_BaseComponent(defPtr);
            }
            if (defaultValue is BaseComponent)
            {
                return Noesis_CreatePropertyMetadata_BaseComponent(
                    BaseComponent.getCPtr((BaseComponent)defaultValue).Handle);
            }

            throw new System.Exception("Default value type not supported");
        }

    #if UNITY_EDITOR

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static void RegisterFunctions(Library lib)
        {
            _CreatePropertyMetadata_Bool = lib.Find<CreatePropertyMetadataDelegate_Bool>(
                "Noesis_CreatePropertyMetadata_Bool");
            _CreatePropertyMetadata_Float = lib.Find<CreatePropertyMetadataDelegate_Float>(
                "Noesis_CreatePropertyMetadata_Float");
            _CreatePropertyMetadata_Int = lib.Find<CreatePropertyMetadataDelegate_Int>(
                "Noesis_CreatePropertyMetadata_Int");
            _CreatePropertyMetadata_UInt = lib.Find<CreatePropertyMetadataDelegate_UInt>(
                "Noesis_CreatePropertyMetadata_UInt");
            _CreatePropertyMetadata_Short = lib.Find<CreatePropertyMetadataDelegate_Short>(
                "Noesis_CreatePropertyMetadata_Short");
            _CreatePropertyMetadata_UShort = lib.Find<CreatePropertyMetadataDelegate_UShort>(
                "Noesis_CreatePropertyMetadata_UShort");
            _CreatePropertyMetadata_String = lib.Find<CreatePropertyMetadataDelegate_String>(
                "Noesis_CreatePropertyMetadata_String");
            _CreatePropertyMetadata_Color = lib.Find<CreatePropertyMetadataDelegate_Color>(
                "Noesis_CreatePropertyMetadata_Color");
            _CreatePropertyMetadata_Point = lib.Find<CreatePropertyMetadataDelegate_Point>(
                "Noesis_CreatePropertyMetadata_Point");
            _CreatePropertyMetadata_Rect = lib.Find<CreatePropertyMetadataDelegate_Rect>(
                "Noesis_CreatePropertyMetadata_Rect");
            _CreatePropertyMetadata_Size = lib.Find<CreatePropertyMetadataDelegate_Size>(
                "Noesis_CreatePropertyMetadata_Size");
            _CreatePropertyMetadata_Thickness = lib.Find<CreatePropertyMetadataDelegate_Thickness>(
                "Noesis_CreatePropertyMetadata_Thickness");
            _CreatePropertyMetadata_BaseComponent = lib.Find<CreatePropertyMetadataDelegate_BaseComponent>(
                "Noesis_CreatePropertyMetadata_BaseComponent");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public static void UnregisterFunctions()
        {
            _CreatePropertyMetadata_Bool = null;
            _CreatePropertyMetadata_Float = null;
            _CreatePropertyMetadata_Int = null;
            _CreatePropertyMetadata_UInt = null;
            _CreatePropertyMetadata_Short = null;
            _CreatePropertyMetadata_UShort = null;
            _CreatePropertyMetadata_String = null;
            _CreatePropertyMetadata_Color = null;
            _CreatePropertyMetadata_Point = null;
            _CreatePropertyMetadata_Rect = null;
            _CreatePropertyMetadata_Size = null;
            _CreatePropertyMetadata_Thickness = null;
            _CreatePropertyMetadata_BaseComponent = null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Bool(bool defaultValue);
        static CreatePropertyMetadataDelegate_Bool _CreatePropertyMetadata_Bool;
        private static IntPtr Noesis_CreatePropertyMetadata_Bool([MarshalAs(UnmanagedType.U1)] bool defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Bool(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Float(float defaultValue);
        static CreatePropertyMetadataDelegate_Float _CreatePropertyMetadata_Float;
        private static IntPtr Noesis_CreatePropertyMetadata_Float(float defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Float(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Int(int defaultValue);
        static CreatePropertyMetadataDelegate_Int _CreatePropertyMetadata_Int;
        private static IntPtr Noesis_CreatePropertyMetadata_Int(int defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Int(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_UInt(uint defaultValue);
        static CreatePropertyMetadataDelegate_UInt _CreatePropertyMetadata_UInt;
        private static IntPtr Noesis_CreatePropertyMetadata_UInt(uint defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_UInt(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Short(short defaultValue);
        static CreatePropertyMetadataDelegate_Short _CreatePropertyMetadata_Short;
        private static IntPtr Noesis_CreatePropertyMetadata_Short(short defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Short(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_UShort(ushort defaultValue);
        static CreatePropertyMetadataDelegate_UShort _CreatePropertyMetadata_UShort;
        private static IntPtr Noesis_CreatePropertyMetadata_UShort(ushort defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_UShort(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_String(IntPtr defaultValue);
        static CreatePropertyMetadataDelegate_String _CreatePropertyMetadata_String;
        private static IntPtr Noesis_CreatePropertyMetadata_String(IntPtr defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_String(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Color(IntPtr defaultValue);
        static CreatePropertyMetadataDelegate_Color _CreatePropertyMetadata_Color;
        private static IntPtr Noesis_CreatePropertyMetadata_Color(IntPtr defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Color(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Point(IntPtr defaultValue);
        static CreatePropertyMetadataDelegate_Point _CreatePropertyMetadata_Point;
        private static IntPtr Noesis_CreatePropertyMetadata_Point(IntPtr defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Point(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Rect(IntPtr defaultValue);
        static CreatePropertyMetadataDelegate_Rect _CreatePropertyMetadata_Rect;
        private static IntPtr Noesis_CreatePropertyMetadata_Rect(IntPtr defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Rect(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Size(IntPtr defaultValue);
        static CreatePropertyMetadataDelegate_Size _CreatePropertyMetadata_Size;
        private static IntPtr Noesis_CreatePropertyMetadata_Size(IntPtr defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Size(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_Thickness(IntPtr defaultValue);
        static CreatePropertyMetadataDelegate_Thickness _CreatePropertyMetadata_Thickness;
        private static IntPtr Noesis_CreatePropertyMetadata_Thickness(IntPtr defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_Thickness(defaultValue);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreatePropertyMetadataDelegate_BaseComponent(IntPtr defaultValue);
        static CreatePropertyMetadataDelegate_BaseComponent _CreatePropertyMetadata_BaseComponent;
        private static IntPtr Noesis_CreatePropertyMetadata_BaseComponent(IntPtr defaultValue)
        {
            IntPtr result = _CreatePropertyMetadata_BaseComponent(defaultValue);
            Error.Check();
            return result;
        }

    #else

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Bool")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Bool")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Bool([MarshalAs(UnmanagedType.U1)]bool defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Float")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Float")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Float(float defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Int")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Int")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Int(int defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_UInt")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_UInt")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_UInt(uint defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Short")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Short")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Short(short defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_UShort")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_UShort")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_UShort(ushort defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_String")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_String")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_String(IntPtr defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Color")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Color")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Color(IntPtr defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Point")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Point")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Point(IntPtr defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Rect")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Rect")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Rect(IntPtr defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Size")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Size")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Size(IntPtr defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_Thickness")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_Thickness")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_Thickness(IntPtr defaultValue);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreatePropertyMetadata_BaseComponent")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreatePropertyMetadata_BaseComponent")]
        #endif
        private static extern IntPtr Noesis_CreatePropertyMetadata_BaseComponent(IntPtr defaultValue);

    #endif

    }

}