/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class BaseTextBox : Control {

  internal BaseTextBox(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.BaseTextBox_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(BaseTextBox obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  protected BaseTextBox() {
  }


  public delegate void SelectionChangedDelegate(BaseComponent sender, RoutedEventArgs e);
  public event SelectionChangedDelegate SelectionChanged {
    add {
      if (!_SelectionChanged.ContainsKey(swigCPtr.Handle)) {
        _SelectionChanged.Add(swigCPtr.Handle, null);

        NoesisGUI_PINVOKE.BindEvent_BaseTextBox_SelectionChanged(RaiseSelectionChanged, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif
      }

      _SelectionChanged[swigCPtr.Handle] += value;
    }
    remove {
      if (!_SelectionChanged.ContainsKey(swigCPtr.Handle)) {
        throw new System.Exception("Delegate not found");
      }

      _SelectionChanged[swigCPtr.Handle] -= value;

      if (_SelectionChanged[swigCPtr.Handle] == null) {
        NoesisGUI_PINVOKE.UnbindEvent_BaseTextBox_SelectionChanged(RaiseSelectionChanged, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif

        _SelectionChanged.Remove(swigCPtr.Handle);
      }
    }
  }

  internal delegate void DelegateRaiseSelectionChanged(IntPtr cPtr, IntPtr sender, IntPtr e);

  [MonoPInvokeCallback(typeof(DelegateRaiseSelectionChanged))]
  private static void RaiseSelectionChanged(IntPtr cPtr, IntPtr sender, IntPtr e) {
    if (!_SelectionChanged.ContainsKey(cPtr)) {
      throw new System.Exception("Delegate not found");
    }
    if (sender == System.IntPtr.Zero && e == System.IntPtr.Zero) {
      _SelectionChanged.Remove(cPtr);
      return;
    }
    if (_SelectionChanged[cPtr] != null) {
      _SelectionChanged[cPtr](new BaseComponent(sender, false), new RoutedEventArgs(e, false));
    }
  }

  static System.Collections.Generic.Dictionary<System.IntPtr, SelectionChangedDelegate> _SelectionChanged =
      new System.Collections.Generic.Dictionary<System.IntPtr, SelectionChangedDelegate>();


  public delegate void TextChangedDelegate(BaseComponent sender, RoutedEventArgs e);
  public event TextChangedDelegate TextChanged {
    add {
      if (!_TextChanged.ContainsKey(swigCPtr.Handle)) {
        _TextChanged.Add(swigCPtr.Handle, null);

        NoesisGUI_PINVOKE.BindEvent_BaseTextBox_TextChanged(RaiseTextChanged, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif
      }

      _TextChanged[swigCPtr.Handle] += value;
    }
    remove {
      if (!_TextChanged.ContainsKey(swigCPtr.Handle)) {
        throw new System.Exception("Delegate not found");
      }

      _TextChanged[swigCPtr.Handle] -= value;

      if (_TextChanged[swigCPtr.Handle] == null) {
        NoesisGUI_PINVOKE.UnbindEvent_BaseTextBox_TextChanged(RaiseTextChanged, swigCPtr.Handle);
        #if UNITY_EDITOR
        if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
        #endif

        _TextChanged.Remove(swigCPtr.Handle);
      }
    }
  }

  internal delegate void DelegateRaiseTextChanged(IntPtr cPtr, IntPtr sender, IntPtr e);

  [MonoPInvokeCallback(typeof(DelegateRaiseTextChanged))]
  private static void RaiseTextChanged(IntPtr cPtr, IntPtr sender, IntPtr e) {
    if (!_TextChanged.ContainsKey(cPtr)) {
      throw new System.Exception("Delegate not found");
    }
    if (sender == System.IntPtr.Zero && e == System.IntPtr.Zero) {
      _TextChanged.Remove(cPtr);
      return;
    }
    if (_TextChanged[cPtr] != null) {
      _TextChanged[cPtr](new BaseComponent(sender, false), new RoutedEventArgs(e, false));
    }
  }

  static System.Collections.Generic.Dictionary<System.IntPtr, TextChangedDelegate> _TextChanged =
      new System.Collections.Generic.Dictionary<System.IntPtr, TextChangedDelegate>();


  public bool GetAcceptsReturn() {
    bool ret = NoesisGUI_PINVOKE.BaseTextBox_GetAcceptsReturn(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetAcceptsReturn(bool value) {
    NoesisGUI_PINVOKE.BaseTextBox_SetAcceptsReturn(swigCPtr, value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public bool GetAcceptsTab() {
    bool ret = NoesisGUI_PINVOKE.BaseTextBox_GetAcceptsTab(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetAcceptsTab(bool value) {
    NoesisGUI_PINVOKE.BaseTextBox_SetAcceptsTab(swigCPtr, value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public bool GetAutoWordSelection() {
    bool ret = NoesisGUI_PINVOKE.BaseTextBox_GetAutoWordSelection(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetAutoWordSelection(bool value) {
    NoesisGUI_PINVOKE.BaseTextBox_SetAutoWordSelection(swigCPtr, value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Brush GetCaretBrush() {
    IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_GetCaretBrush(swigCPtr);
    Brush ret = (cPtr == IntPtr.Zero) ? null : new Brush(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetCaretBrush(Brush brush) {
    NoesisGUI_PINVOKE.BaseTextBox_SetCaretBrush(swigCPtr, Brush.getCPtr(brush));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public ScrollBarVisibility GetHorizontalScrollBarVisibility() {
    ScrollBarVisibility ret = (ScrollBarVisibility)NoesisGUI_PINVOKE.BaseTextBox_GetHorizontalScrollBarVisibility(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetHorizontalScrollBarVisibility(ScrollBarVisibility value) {
    NoesisGUI_PINVOKE.BaseTextBox_SetHorizontalScrollBarVisibility(swigCPtr, (int)value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public bool GetIsReadOnly() {
    bool ret = NoesisGUI_PINVOKE.BaseTextBox_GetIsReadOnly(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetIsReadOnly(bool value) {
    NoesisGUI_PINVOKE.BaseTextBox_SetIsReadOnly(swigCPtr, value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public bool GetIsUndoEnabled() {
    bool ret = NoesisGUI_PINVOKE.BaseTextBox_GetIsUndoEnabled(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetIsUndoEnabled(bool value) {
    NoesisGUI_PINVOKE.BaseTextBox_SetIsUndoEnabled(swigCPtr, value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Brush GetSelectionBrush() {
    IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_GetSelectionBrush(swigCPtr);
    Brush ret = (cPtr == IntPtr.Zero) ? null : new Brush(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetSelectionBrush(Brush selectionBrush) {
    NoesisGUI_PINVOKE.BaseTextBox_SetSelectionBrush(swigCPtr, Brush.getCPtr(selectionBrush));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public float GetSelectionOpacity() {
    float ret = NoesisGUI_PINVOKE.BaseTextBox_GetSelectionOpacity(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetSelectionOpacity(float selectionOpacity) {
    NoesisGUI_PINVOKE.BaseTextBox_SetSelectionOpacity(swigCPtr, selectionOpacity);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public int GetUndoLimit() {
    int ret = NoesisGUI_PINVOKE.BaseTextBox_GetUndoLimit(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetUndoLimit(int limit) {
    NoesisGUI_PINVOKE.BaseTextBox_SetUndoLimit(swigCPtr, limit);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public ScrollBarVisibility GetVerticalScrollBarVisibility() {
    ScrollBarVisibility ret = (ScrollBarVisibility)NoesisGUI_PINVOKE.BaseTextBox_GetVerticalScrollBarVisibility(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetVerticalScrollBarVisibility(ScrollBarVisibility value) {
    NoesisGUI_PINVOKE.BaseTextBox_SetVerticalScrollBarVisibility(swigCPtr, (int)value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty AcceptsReturnProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_AcceptsReturnProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty AcceptsTabProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_AcceptsTabProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty AutoWordSelectionProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_AutoWordSelectionProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty CaretBrushProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_CaretBrushProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty HorizontalScrollBarVisibilityProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_HorizontalScrollBarVisibilityProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty IsReadOnlyProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_IsReadOnlyProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty IsUndoEnabledProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_IsUndoEnabledProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty SelectionBrushProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_SelectionBrushProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty SelectionOpacityProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_SelectionOpacityProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty UndoLimitProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_UndoLimitProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty VerticalScrollBarVisibilityProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.BaseTextBox_VerticalScrollBarVisibilityProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static void Extend(System.Type type) {
    IntPtr typePtr = Noesis.Extend.GetPtrForType(type);
    NoesisGUI_PINVOKE.Extend_BaseTextBox(typePtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_BaseTextBox(valPtr);
  }

}

}

