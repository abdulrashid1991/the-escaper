/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class ControllableStoryboardAction : TriggerAction {

  internal ControllableStoryboardAction(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.ControllableStoryboardAction_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(ControllableStoryboardAction obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  protected ControllableStoryboardAction() {
  }

  public string GetBeginStoryboardName() {
    string ret = NoesisGUI_PINVOKE.ControllableStoryboardAction_GetBeginStoryboardName(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetBeginStoryboardName(string name) {
    NoesisGUI_PINVOKE.ControllableStoryboardAction_SetBeginStoryboardName(swigCPtr, name);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public override void Invoke(FrameworkElement target, FrameworkElement nameScope) {
    NoesisGUI_PINVOKE.ControllableStoryboardAction_Invoke__SWIG_0(swigCPtr, FrameworkElement.getCPtr(target), FrameworkElement.getCPtr(nameScope));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public override void Invoke(FrameworkElement target) {
    NoesisGUI_PINVOKE.ControllableStoryboardAction_Invoke__SWIG_1(swigCPtr, FrameworkElement.getCPtr(target));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_ControllableStoryboardAction(valPtr);
  }

}

}

