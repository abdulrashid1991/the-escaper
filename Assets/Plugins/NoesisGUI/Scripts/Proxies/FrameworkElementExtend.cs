using System;
using System.Runtime.InteropServices;

namespace Noesis
{

    public partial class FrameworkElement
    {
        public T FindName<T>(string name) where T : BaseComponent
        {
            BaseComponent element = FindName(name);
            return element == null ? null : element.As<T>();
        }

        public T FindStringResource<T>(string resourceKeyString) where T : BaseComponent
        {
            IntPtr stringPtr = Marshal.StringToHGlobalAnsi(resourceKeyString);
            IntPtr cPtr = NoesisGUI_PINVOKE.FrameworkElement_FindStringResource(swigCPtr, stringPtr);

            BaseComponent baseComponent = cPtr == IntPtr.Zero ? null : new BaseComponent(cPtr, false);

            return baseComponent == null ? null : baseComponent.As<T>();
        }

        public T FindTypeResource<T>(string resourceKeyType) where T : BaseComponent
        {
            IntPtr stringPtr = Marshal.StringToHGlobalAnsi(resourceKeyType);
            IntPtr cPtr = NoesisGUI_PINVOKE.FrameworkElement_FindTypeResource(swigCPtr, stringPtr);

            BaseComponent baseComponent = cPtr == IntPtr.Zero ? null : new BaseComponent(cPtr, false);

            return baseComponent == null ? null : baseComponent.As<T>();
        }  
    }

}