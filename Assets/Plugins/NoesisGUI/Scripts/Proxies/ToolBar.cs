/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class ToolBar : HeaderedItemsControl {

  internal ToolBar(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.ToolBar_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(ToolBar obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public ToolBar() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(ToolBar)) {
      return NoesisGUI_PINVOKE.new_ToolBar();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public static OverflowMode GetOverflowMode(DependencyObject element) {
    OverflowMode ret = (OverflowMode)NoesisGUI_PINVOKE.ToolBar_GetOverflowMode(DependencyObject.getCPtr(element));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static void SetOverflowMode(DependencyObject element, OverflowMode mode) {
    NoesisGUI_PINVOKE.ToolBar_SetOverflowMode(DependencyObject.getCPtr(element), (int)mode);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static bool GetIsOverflowItem(DependencyObject element) {
    bool ret = NoesisGUI_PINVOKE.ToolBar_GetIsOverflowItem(DependencyObject.getCPtr(element));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static void SetIsOverflowItem(DependencyObject element, bool isOverflowItem) {
    NoesisGUI_PINVOKE.ToolBar_SetIsOverflowItem(DependencyObject.getCPtr(element), isOverflowItem);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public int GetBandIndex() {
    int ret = NoesisGUI_PINVOKE.ToolBar_GetBandIndex(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetBandIndex(int value) {
    NoesisGUI_PINVOKE.ToolBar_SetBandIndex(swigCPtr, value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public int GetBand() {
    int ret = NoesisGUI_PINVOKE.ToolBar_GetBand(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetBand(int value) {
    NoesisGUI_PINVOKE.ToolBar_SetBand(swigCPtr, value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public bool GetHasOverflowItems() {
    bool ret = NoesisGUI_PINVOKE.ToolBar_GetHasOverflowItems(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public bool GetIsOverflowOpen() {
    bool ret = NoesisGUI_PINVOKE.ToolBar_GetIsOverflowOpen(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public Orientation GetOrientation() {
    Orientation ret = (Orientation)NoesisGUI_PINVOKE.ToolBar_GetOrientation(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public ToolBarPanel GetPanel() {
    IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_GetPanel(swigCPtr);
    ToolBarPanel ret = (cPtr == IntPtr.Zero) ? null : new ToolBarPanel(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public ToolBarOverflowPanel GetOverflowPanel() {
    IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_GetOverflowPanel(swigCPtr);
    ToolBarOverflowPanel ret = (cPtr == IntPtr.Zero) ? null : new ToolBarOverflowPanel(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void UpdateHasOverflowItems(bool hasOverflowItems) {
    NoesisGUI_PINVOKE.ToolBar_UpdateHasOverflowItems(swigCPtr, hasOverflowItems);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty BandIndexProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_BandIndexProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty BandProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_BandProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty HasOverflowItemsProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_HasOverflowItemsProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty IsOverflowItemProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_IsOverflowItemProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty IsOverflowOpenProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_IsOverflowOpenProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty OrientationProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_OrientationProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty OverflowModeProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_OverflowModeProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ButtonStyleKey {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_ButtonStyleKey_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ToggleButtonStyleKey {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_ToggleButtonStyleKey_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty CheckBoxStyleKey {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_CheckBoxStyleKey_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty RadioButtonStyleKey {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_RadioButtonStyleKey_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty TextBoxStyleKey {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_TextBoxStyleKey_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ComboBoxStyleKey {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_ComboBoxStyleKey_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty SeparatorStyleKey {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_SeparatorStyleKey_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty MenuStyleKey {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.ToolBar_MenuStyleKey_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static void Extend(System.Type type) {
    IntPtr typePtr = Noesis.Extend.GetPtrForType(type);
    NoesisGUI_PINVOKE.Extend_ToolBar(typePtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_ToolBar(valPtr);
  }

}

}

