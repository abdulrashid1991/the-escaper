using System;
using System.Runtime.InteropServices;

namespace Noesis
{

    public partial class FrameworkPropertyMetadata
    {
        public FrameworkPropertyMetadata(object defaultValue)
            : this(Create(defaultValue, FrameworkOptions.None), true)
        {
        }

        public FrameworkPropertyMetadata(object defaultValue, FrameworkOptions options)
            : this(Create(defaultValue, options), true)
        {
        }

        private static IntPtr Create(object defaultValue, FrameworkOptions options)
        {
            if (defaultValue == null)
            {
                return Noesis_CreateFrameworkPropertyMetadata_BaseComponent(IntPtr.Zero,
                    (int)options);
            }
            if (defaultValue is bool)
            {
                return Noesis_CreateFrameworkPropertyMetadata_Bool((bool)defaultValue,
                    (int)options);
            }
            if (defaultValue is float)
            {
                return Noesis_CreateFrameworkPropertyMetadata_Float((float)defaultValue,
                    (int)options);
            }
            if (defaultValue is int)
            {
                return Noesis_CreateFrameworkPropertyMetadata_Int((int)defaultValue,
                    (int)options);
            }
            if (defaultValue is uint)
            {
                return Noesis_CreateFrameworkPropertyMetadata_UInt((uint)defaultValue,
                    (int)options);
            }
            if (defaultValue is short)
            {
                return Noesis_CreateFrameworkPropertyMetadata_Short((short)defaultValue,
                    (int)options);
            }
            if (defaultValue is ushort)
            {
                return Noesis_CreateFrameworkPropertyMetadata_UShort((ushort)defaultValue,
                    (int)options);
            }
            if (defaultValue.GetType().IsEnum)
            {
                defaultValue = defaultValue.ToString();
            }
            if (defaultValue is string)
            {
                IntPtr strPtr = Marshal.StringToHGlobalAnsi((string)defaultValue);
                return Noesis_CreateFrameworkPropertyMetadata_String(strPtr,
                    (int)options);
            }
            if (defaultValue is Color)
            {
                IntPtr colorPtr = Color.getCPtr((Color)defaultValue).Handle;
                return Noesis_CreateFrameworkPropertyMetadata_Color(colorPtr,
                    (int)options);
            }
            if (defaultValue is Point)
            {
                IntPtr pointPtr = Point.getCPtr((Point)defaultValue).Handle;
                return Noesis_CreateFrameworkPropertyMetadata_Point(pointPtr,
                    (int)options);
            }
            if (defaultValue is Rect)
            {
                IntPtr rectPtr = Rect.getCPtr((Rect)defaultValue).Handle;
                return Noesis_CreateFrameworkPropertyMetadata_Rect(rectPtr,
                    (int)options);
            }
            if (defaultValue is Size)
            {
                IntPtr sizePtr = Size.getCPtr((Size)defaultValue).Handle;
                return Noesis_CreateFrameworkPropertyMetadata_Size(sizePtr,
                    (int)options);
            }
            if (defaultValue is Thickness)
            {
                IntPtr thicknessPtr = Thickness.getCPtr((Thickness)defaultValue).Handle;
                return Noesis_CreateFrameworkPropertyMetadata_Thickness(thicknessPtr,
                    (int)options);
            }
            if (defaultValue is Type)
            {
                IntPtr defPtr = Noesis.Extend.GetResourceKeyType(defaultValue as Type);
                return Noesis_CreateFrameworkPropertyMetadata_BaseComponent(defPtr,
                    (int)options);
            }
            if (defaultValue is BaseComponent)
            {
                return Noesis_CreateFrameworkPropertyMetadata_BaseComponent(
                    BaseComponent.getCPtr((BaseComponent)defaultValue).Handle,
                    (int)options);
            }

            throw new System.Exception("Default value type not supported");
        }

    #if UNITY_EDITOR

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public new static void RegisterFunctions(Library lib)
        {
            // create FrameworkPropertyMetadata 
            _CreateFrameworkPropertyMetadata_Bool = lib.Find<CreateFrameworkPropertyMetadataDelegate_Bool>(
                "Noesis_CreateFrameworkPropertyMetadata_Bool");
            _CreateFrameworkPropertyMetadata_Float = lib.Find<CreateFrameworkPropertyMetadataDelegate_Float>(
                "Noesis_CreateFrameworkPropertyMetadata_Float");
            _CreateFrameworkPropertyMetadata_Int = lib.Find<CreateFrameworkPropertyMetadataDelegate_Int>(
                "Noesis_CreateFrameworkPropertyMetadata_Int");
            _CreateFrameworkPropertyMetadata_UInt = lib.Find<CreateFrameworkPropertyMetadataDelegate_UInt>(
                "Noesis_CreateFrameworkPropertyMetadata_UInt");
            _CreateFrameworkPropertyMetadata_Short = lib.Find<CreateFrameworkPropertyMetadataDelegate_Short>(
                "Noesis_CreateFrameworkPropertyMetadata_Short");
            _CreateFrameworkPropertyMetadata_UShort = lib.Find<CreateFrameworkPropertyMetadataDelegate_UShort>(
                "Noesis_CreateFrameworkPropertyMetadata_UShort");
            _CreateFrameworkPropertyMetadata_String = lib.Find<CreateFrameworkPropertyMetadataDelegate_String>(
                "Noesis_CreateFrameworkPropertyMetadata_String");
            _CreateFrameworkPropertyMetadata_Color = lib.Find<CreateFrameworkPropertyMetadataDelegate_Color>(
                "Noesis_CreateFrameworkPropertyMetadata_Color");
            _CreateFrameworkPropertyMetadata_Point = lib.Find<CreateFrameworkPropertyMetadataDelegate_Point>(
                "Noesis_CreateFrameworkPropertyMetadata_Point");
            _CreateFrameworkPropertyMetadata_Rect = lib.Find<CreateFrameworkPropertyMetadataDelegate_Rect>(
                "Noesis_CreateFrameworkPropertyMetadata_Rect");
            _CreateFrameworkPropertyMetadata_Size = lib.Find<CreateFrameworkPropertyMetadataDelegate_Size>(
                "Noesis_CreateFrameworkPropertyMetadata_Size");
            _CreateFrameworkPropertyMetadata_Thickness = lib.Find<CreateFrameworkPropertyMetadataDelegate_Thickness>(
                "Noesis_CreateFrameworkPropertyMetadata_Thickness");
            _CreateFrameworkPropertyMetadata_BaseComponent = lib.Find<CreateFrameworkPropertyMetadataDelegate_BaseComponent>(
                "Noesis_CreateFrameworkPropertyMetadata_BaseComponent");
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        public new static void UnregisterFunctions()
        {
            // create FrameworkPropertyMetadata 
            _CreateFrameworkPropertyMetadata_Bool = null;
            _CreateFrameworkPropertyMetadata_Float = null;
            _CreateFrameworkPropertyMetadata_Int = null;
            _CreateFrameworkPropertyMetadata_UInt = null;
            _CreateFrameworkPropertyMetadata_Short = null;
            _CreateFrameworkPropertyMetadata_UShort = null;
            _CreateFrameworkPropertyMetadata_String = null;
            _CreateFrameworkPropertyMetadata_Color = null;
            _CreateFrameworkPropertyMetadata_Point = null;
            _CreateFrameworkPropertyMetadata_Rect = null;
            _CreateFrameworkPropertyMetadata_Size = null;
            _CreateFrameworkPropertyMetadata_Thickness = null;
            _CreateFrameworkPropertyMetadata_BaseComponent = null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Bool(bool defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Bool _CreateFrameworkPropertyMetadata_Bool;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Bool(bool defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Bool(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Float(float defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Float _CreateFrameworkPropertyMetadata_Float;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Float(float defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Float(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Int(int defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Int _CreateFrameworkPropertyMetadata_Int;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Int(int defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Int(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_UInt(uint defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_UInt _CreateFrameworkPropertyMetadata_UInt;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_UInt(uint defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_UInt(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Short(short defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Short _CreateFrameworkPropertyMetadata_Short;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Short(short defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Short(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_UShort(ushort defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_UShort _CreateFrameworkPropertyMetadata_UShort;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_UShort(ushort defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_UShort(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_String(IntPtr defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_String _CreateFrameworkPropertyMetadata_String;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_String(IntPtr defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_String(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Color(IntPtr defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Color _CreateFrameworkPropertyMetadata_Color;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Color(IntPtr defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Color(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Point(IntPtr defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Point _CreateFrameworkPropertyMetadata_Point;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Point(IntPtr defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Point(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Rect(IntPtr defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Rect _CreateFrameworkPropertyMetadata_Rect;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Rect(IntPtr defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Rect(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Size(IntPtr defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Size _CreateFrameworkPropertyMetadata_Size;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Size(IntPtr defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Size(defaultValue, options);
            Error.Check();
            return result;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_Thickness(IntPtr defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_Thickness _CreateFrameworkPropertyMetadata_Thickness;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_Thickness(IntPtr defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_Thickness(defaultValue, options);
            Error.Check();
            return result;
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateFrameworkPropertyMetadataDelegate_BaseComponent(IntPtr defaultValue, int options);
        static CreateFrameworkPropertyMetadataDelegate_BaseComponent _CreateFrameworkPropertyMetadata_BaseComponent;
        private static IntPtr Noesis_CreateFrameworkPropertyMetadata_BaseComponent(IntPtr defaultValue, int options)
        {
            IntPtr result = _CreateFrameworkPropertyMetadata_BaseComponent(defaultValue, options);
            Error.Check();
            return result;
        }

    #else

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Bool")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Bool")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Bool([MarshalAs(UnmanagedType.U1)]bool defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Float")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Float")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Float(float defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Int")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Int")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Int(int defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_UInt")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_UInt")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_UInt(uint defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Short")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Short")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Short(short defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_UShort")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_UShort")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_UShort(ushort defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_String")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_String")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_String(IntPtr defaultValue, int options);
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Color")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Color")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Color(IntPtr defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Point")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Point")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Point(IntPtr defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Rect")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Rect")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Rect(IntPtr defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Size")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Size")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Size(IntPtr defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_Thickness")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_Thickness")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_Thickness(IntPtr defaultValue, int options);

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateFrameworkPropertyMetadata_BaseComponent")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateFrameworkPropertyMetadata_BaseComponent")]
        #endif
        private static extern IntPtr Noesis_CreateFrameworkPropertyMetadata_BaseComponent(IntPtr defaultValue, int options);

    #endif

    }

}