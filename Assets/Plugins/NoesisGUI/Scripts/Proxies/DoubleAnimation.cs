/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class DoubleAnimation : BaseAnimation {

  internal DoubleAnimation(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.DoubleAnimation_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(DoubleAnimation obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public DoubleAnimation() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(DoubleAnimation)) {
      return NoesisGUI_PINVOKE.new_DoubleAnimation();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public System.Nullable<float> GetBy() {
    NullableFloat ret = new NullableFloat(NoesisGUI_PINVOKE.DoubleAnimation_GetBy(swigCPtr), false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    System.Nullable<float> retNullable = null;
    if (ret.HasValue()) {
      retNullable = ret.GetValue();
    }
    return retNullable;
  }

  public void SetBy(System.Nullable<float> value_) {
    NullableFloat value = new NullableFloat();
    if (value_.HasValue) {
      value.SetValue(value_.Value);
    }

    NoesisGUI_PINVOKE.DoubleAnimation_SetBy(swigCPtr, NullableFloat.getCPtr(value));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public NullableFloat GetFrom() {
    NullableFloat ret = new NullableFloat(NoesisGUI_PINVOKE.DoubleAnimation_GetFrom(swigCPtr), false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetFrom(NullableFloat from) {
    NoesisGUI_PINVOKE.DoubleAnimation_SetFrom(swigCPtr, NullableFloat.getCPtr(from));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public NullableFloat GetTo() {
    NullableFloat ret = new NullableFloat(NoesisGUI_PINVOKE.DoubleAnimation_GetTo(swigCPtr), false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetTo(NullableFloat to) {
    NoesisGUI_PINVOKE.DoubleAnimation_SetTo(swigCPtr, NullableFloat.getCPtr(to));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public static DependencyProperty ByProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.DoubleAnimation_ByProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty FromProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.DoubleAnimation_FromProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ToProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.DoubleAnimation_ToProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_DoubleAnimation(valPtr);
  }

}

}

