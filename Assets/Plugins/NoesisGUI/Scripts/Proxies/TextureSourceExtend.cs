using System;
using System.Runtime.InteropServices;

namespace Noesis
{

    public partial class TextureSource
    {
        public TextureSource(UnityEngine.Texture texture)
            : this(Create(texture, 1), true)
        {
        }

        public TextureSource(UnityEngine.Texture2D texture)
            : this(Create(texture, texture.mipmapCount), true)
        {
        }

        private static IntPtr Create(UnityEngine.Texture texture, int mipmapCount)
        {
            if (texture == null)
            {
                throw new System.Exception("Can't create TextureSource, Unity texture is null");
            }

            IntPtr nativeTexturePtr = texture.GetNativeTexturePtr();
            if (nativeTexturePtr == IntPtr.Zero)
            {
                throw new System.Exception("Can't create TextureSource, texture native pointer is null");
            }

            return Noesis_CreateTextureSource(nativeTexturePtr, texture.width, texture.height, mipmapCount);
        }

    #if UNITY_EDITOR

        public new static void RegisterFunctions(Library lib)
        {
            _CreateTextureSource = lib.Find<CreateTextureSourceDelegate>("Noesis_CreateTextureSource");
        }

        public new static void UnregisterFunctions()
        {
            _CreateTextureSource = null;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
        delegate IntPtr CreateTextureSourceDelegate(IntPtr texture, int width, int height, int numMipMaps);
        static CreateTextureSourceDelegate _CreateTextureSource;
        static IntPtr Noesis_CreateTextureSource(IntPtr texture, int width, int height, int numMipMaps)
        {
            IntPtr ret = _CreateTextureSource(texture, width, height, numMipMaps);
            Error.Check();
            return ret;
        }

    #else

        ////////////////////////////////////////////////////////////////////////////////////////////////
        #if UNITY_IPHONE || UNITY_XBOX360
        [DllImport("__Internal", EntryPoint="Noesis_CreateTextureSource")]
        #else
        [DllImport("Noesis", EntryPoint = "Noesis_CreateTextureSource")]
        #endif
        static extern IntPtr Noesis_CreateTextureSource(IntPtr texture, int width, int height, int numMipMaps);

#endif

    }

}