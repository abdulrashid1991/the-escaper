/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class GridView : BaseView {

  internal GridView(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.GridView_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(GridView obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public GridView() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(GridView)) {
      return NoesisGUI_PINVOKE.new_GridView();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public bool GetAllowsColumnReorder() {
    bool ret = NoesisGUI_PINVOKE.GridView_GetAllowsColumnReorder(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetAllowsColumnReorder(bool value) {
    NoesisGUI_PINVOKE.GridView_SetAllowsColumnReorder(swigCPtr, value);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Style GetColumnHeaderContainerStyle() {
    IntPtr cPtr = NoesisGUI_PINVOKE.GridView_GetColumnHeaderContainerStyle(swigCPtr);
    Style ret = (cPtr == IntPtr.Zero) ? null : new Style(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetColumnHeaderContainerStyle(Style style) {
    NoesisGUI_PINVOKE.GridView_SetColumnHeaderContainerStyle(swigCPtr, Style.getCPtr(style));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public ContextMenu GetColumnHeaderContextMenu() {
    IntPtr cPtr = NoesisGUI_PINVOKE.GridView_GetColumnHeaderContextMenu(swigCPtr);
    ContextMenu ret = (cPtr == IntPtr.Zero) ? null : new ContextMenu(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetColumnHeaderContextMenu(ContextMenu menu) {
    NoesisGUI_PINVOKE.GridView_SetColumnHeaderContextMenu(swigCPtr, ContextMenu.getCPtr(menu));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public string GetColumnHeaderStringFormat() {
    IntPtr cPtr = NoesisGUI_PINVOKE.GridView_GetColumnHeaderStringFormat(swigCPtr);
    NsString nsstring = new NsString(cPtr, false);
    string ret = nsstring.c_str();
    return ret;
}

  public void SetColumnHeaderStringFormat(string format) {
    NoesisGUI_PINVOKE.GridView_SetColumnHeaderStringFormat(swigCPtr, format != null ? Marshal.StringToHGlobalAnsi(format) : Marshal.StringToHGlobalAnsi(string.Empty));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public DataTemplate GetColumnHeaderTemplate() {
    IntPtr cPtr = NoesisGUI_PINVOKE.GridView_GetColumnHeaderTemplate(swigCPtr);
    DataTemplate ret = (cPtr == IntPtr.Zero) ? null : new DataTemplate(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetColumnHeaderTemplate(DataTemplate dataTemplate) {
    NoesisGUI_PINVOKE.GridView_SetColumnHeaderTemplate(swigCPtr, DataTemplate.getCPtr(dataTemplate));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public DataTemplateSelector GetColumnHeaderTemplateSelector() {
    IntPtr cPtr = NoesisGUI_PINVOKE.GridView_GetColumnHeaderTemplateSelector(swigCPtr);
    DataTemplateSelector ret = (cPtr == IntPtr.Zero) ? null : new DataTemplateSelector(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetColumnHeaderTemplateSelector(DataTemplateSelector selector) {
    NoesisGUI_PINVOKE.GridView_SetColumnHeaderTemplateSelector(swigCPtr, DataTemplateSelector.getCPtr(selector));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public BaseComponent GetColumnHeaderToolTip() {
    IntPtr cPtr = NoesisGUI_PINVOKE.GridView_GetColumnHeaderToolTip(swigCPtr);
    BaseComponent ret = (cPtr == IntPtr.Zero) ? null : new BaseComponent(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetColumnHeaderToolTip(BaseComponent tooltip) {
    NoesisGUI_PINVOKE.GridView_SetColumnHeaderToolTip(swigCPtr, BaseComponent.getCPtr(tooltip));
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public GridViewColumnCollection GetColumns() {
    IntPtr cPtr = NoesisGUI_PINVOKE.GridView_GetColumns(swigCPtr);
    GridViewColumnCollection ret = (cPtr == IntPtr.Zero) ? null : new GridViewColumnCollection(cPtr, false);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static DependencyProperty AllowsColumnReorderProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.GridView_AllowsColumnReorderProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ColumnHeaderContainerStyleProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.GridView_ColumnHeaderContainerStyleProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ColumnHeaderContextMenuProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.GridView_ColumnHeaderContextMenuProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ColumnHeaderStringFormatProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.GridView_ColumnHeaderStringFormatProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ColumnHeaderTemplateProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.GridView_ColumnHeaderTemplateProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ColumnHeaderTemplateSelectorProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.GridView_ColumnHeaderTemplateSelectorProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ColumnHeaderToolTipProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.GridView_ColumnHeaderToolTipProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty ColumnCollectionProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.GridView_ColumnCollectionProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_GridView(valPtr);
  }

}

}

