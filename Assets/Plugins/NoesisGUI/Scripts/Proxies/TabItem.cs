/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.4
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */


using System;
using System.Runtime.InteropServices;

namespace Noesis
{

public class TabItem : HeaderedContentControl {

  internal TabItem(IntPtr cPtr, bool cMemoryOwn) : base(NoesisGUI_PINVOKE.TabItem_SWIGUpcast(cPtr), cMemoryOwn) {
  }

  internal static HandleRef getCPtr(TabItem obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  public TabItem() {
  }

  protected override System.IntPtr CreateCPtr(System.Type type) {
    if (type == typeof(TabItem)) {
      return NoesisGUI_PINVOKE.new_TabItem();
    }
    else {
      return base.CreateCPtr(type);
    }
  }

  public bool GetIsSelected() {
    bool ret = NoesisGUI_PINVOKE.TabItem_GetIsSelected(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public void SetIsSelected(bool isSelected) {
    NoesisGUI_PINVOKE.TabItem_SetIsSelected(swigCPtr, isSelected);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  public Dock GetTabStripPlacement() {
    Dock ret = (Dock)NoesisGUI_PINVOKE.TabItem_GetTabStripPlacement(swigCPtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
    return ret;
  }

  public static DependencyProperty IsSelectedProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.TabItem_IsSelectedProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }

  public static DependencyProperty TabStripPlacementProperty {
    get {
      IntPtr cPtr = NoesisGUI_PINVOKE.TabItem_TabStripPlacementProperty_get();
      DependencyProperty ret = (cPtr == IntPtr.Zero) ? null : new DependencyProperty(cPtr, false);
      #if UNITY_EDITOR
      if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
      #endif
      return ret;
    } 
  }


  internal new static void Extend(System.Type type) {
    IntPtr typePtr = Noesis.Extend.GetPtrForType(type);
    NoesisGUI_PINVOKE.Extend_TabItem(typePtr);
    #if UNITY_EDITOR
    if (NoesisGUI_PINVOKE.SWIGPendingException.Pending) throw NoesisGUI_PINVOKE.SWIGPendingException.Retrieve();
    #endif
  }

  internal new static bool CheckType(BaseComponent val) {
    IntPtr valPtr = BaseComponent.getCPtr(val).Handle;
    return NoesisGUI_PINVOKE.CheckType_TabItem(valPtr);
  }

}

}

