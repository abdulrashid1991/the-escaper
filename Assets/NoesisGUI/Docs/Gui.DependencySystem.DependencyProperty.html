<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="generator" content="Docutils 0.11: http://docutils.sourceforge.net/" />
<title>DependencyProperty</title>
<link rel="stylesheet" href="style/noesis.css" type="text/css" />
</head>
<body>
<div class="document" id="dependencyproperty">
<div>
    <div class="headerbg">
        <center><img src="NoesisLogo.png" class="headerimg"/></center>
    </div>
    <div class="headermenu">
        <a href="Gui.Core.Index.html" class="headerlink">Documentation Index</a>
    </div>
</div>
<h1 class="title">DependencyProperty</h1>

<div class="section" id="introduction">
<h1>Introduction</h1>
<p>The purpose of dependency properties is to provide a way to compute the value of a property based on the value of other inputs. These other inputs might include system properties such as themes and user preference, just-in-time property determination mechanisms such as data binding and animations/storyboards, multiple-use templates such as resources and styles, or values known through parent-child relationships with other elements in the element tree. In addition, a dependency property can be implemented to provide self-contained validation, default values, callbacks that monitor changes to other properties, and a system that can coerce property values based on potentially runtime information. Derived classes can also change some specific characteristics of an existing property by overriding dependency property metadata, rather than overriding the actual implementation of existing properties or creating new properties.</p>
</div>
<div class="section" id="implementation">
<h1>Implementation</h1>
<p>Noesis Engine implements dependency property system over reflection metadata support. Every dependency object stores its dependency properties inside a reflection TypeMetaData derived class, the <strong>DependencyData</strong>. This class provides functionality to register dependency properties associated with a reflection type.</p>
<p>To create a new derived DependencyObject class we begin defining the public dependency properties it will have:</p>
<div class="highlight"><pre><span class="c1">// DelayedButton.h</span>

<span class="c1">////////////////////////////////////////////////////////////////////////////////////////////////////</span>
<span class="k">class</span> <span class="nc">DelayedButton</span><span class="o">:</span> <span class="k">public</span> <span class="n">Button</span>
<span class="p">{</span>
<span class="nl">public:</span>
    <span class="c1">/// Dependency properties</span>
    <span class="c1">//@{</span>
    <span class="k">static</span> <span class="k">const</span> <span class="n">DependencyProperty</span><span class="o">*</span> <span class="n">DelayProperty</span><span class="p">;</span>
    <span class="c1">//@}</span>
<span class="p">};</span>
</pre></div>
<p>Then we must register the property in the dependency system:</p>
<div class="highlight"><pre><span class="c1">// DelayedButton.cpp</span>

<span class="c1">////////////////////////////////////////////////////////////////////////////////////////////////////</span>
<span class="n">NS_IMPLEMENT_REFLECTION</span><span class="p">(</span><span class="n">DelayedButton</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">Ptr</span><span class="o">&lt;</span><span class="n">DependencyData</span><span class="o">&gt;</span> <span class="n">data</span> <span class="o">=</span> <span class="n">NsMeta</span><span class="o">&lt;</span><span class="n">DependencyData</span><span class="o">&gt;</span><span class="p">(</span><span class="n">TypeOf</span><span class="o">&lt;</span><span class="n">SelfClass</span><span class="o">&gt;</span><span class="p">());</span>
    <span class="n">data</span><span class="o">-&gt;</span><span class="n">RegisterProperty</span><span class="o">&lt;</span><span class="n">NsFloat32</span><span class="o">&gt;</span><span class="p">(</span><span class="n">DelayProperty</span><span class="p">,</span> <span class="s">&quot;Delay&quot;</span><span class="p">,</span> <span class="n">PropertyMetaData</span><span class="o">::</span><span class="n">Create</span><span class="p">(</span><span class="mf">1.0f</span><span class="p">));</span>
<span class="p">}</span>
</pre></div>
<p>The previous code does three things:</p>
<blockquote>
<ul class="simple">
<li><strong>NsMeta</strong> creates a metadata associated with the reflection of DelayedButton class, that would be used by dependency system to look for dependency properties registered by this class.</li>
<li><strong>RegisterProperty</strong> creates a property in the dependency system named &quot;Delay&quot;, with a default value of 1.0f, and it is associated to DelayedButton class.</li>
<li>RegisterProperty also assigns the created property to the DelayProperty public member.</li>
</ul>
</blockquote>
<p>After this we can use that class from a xaml like this:</p>
<div class="highlight"><pre><span class="nt">&lt;DelayedButton</span> <span class="na">Delay=</span><span class="s">&quot;1.5&quot;</span><span class="nt">/&gt;</span>
</pre></div>
<p>Or in code:</p>
<div class="highlight"><pre><span class="n">Ptr</span><span class="o">&lt;</span><span class="n">DelayedButton</span><span class="o">&gt;</span> <span class="n">btn</span> <span class="o">=</span> <span class="n">NsCreateComponent</span><span class="o">&lt;</span><span class="n">DelayedButton</span><span class="o">&gt;</span><span class="p">();</span>
<span class="n">btn</span><span class="o">-&gt;</span><span class="n">SetValue</span><span class="o">&lt;</span><span class="n">NsFloat32</span><span class="o">&gt;</span><span class="p">(</span><span class="n">DelayedButton</span><span class="o">::</span><span class="n">DelayProperty</span><span class="p">,</span> <span class="mf">1.5f</span><span class="p">);</span>
</pre></div>
<p>The function <strong>SetValue</strong> is a templated method of DependencyObject that sets the local value of the specified dependency property in the object. To make it easier to modify and access dependency property values, we usually wrap them with getter/setter methods in the owner class (we omit the above code to make it more clear):</p>
<div class="highlight"><pre><span class="c1">// DelayedButton.h</span>

<span class="c1">////////////////////////////////////////////////////////////////////////////////////////////////////</span>
<span class="k">class</span> <span class="nc">DelayedButton</span><span class="o">:</span> <span class="k">public</span> <span class="n">Button</span>
<span class="p">{</span>
<span class="nl">public:</span>
    <span class="c1">/// Gets or sets button click delay in seconds</span>
    <span class="c1">//@{</span>
    <span class="n">NsFloat32</span> <span class="n">GetDelay</span><span class="p">()</span> <span class="k">const</span><span class="p">;</span>
    <span class="kt">void</span> <span class="nf">SetDelay</span><span class="p">(</span><span class="n">NsFloat32</span> <span class="n">delay</span><span class="p">);</span>
    <span class="c1">//@}</span>
<span class="p">};</span>

<span class="c1">// DelayedButton.cpp</span>

<span class="c1">////////////////////////////////////////////////////////////////////////////////////////////////////</span>
<span class="n">NsFloat32</span> <span class="n">DelayedButton</span><span class="o">::</span><span class="n">GetDelay</span><span class="p">()</span> <span class="k">const</span>
<span class="p">{</span>
    <span class="k">return</span> <span class="n">GetValue</span><span class="o">&lt;</span><span class="n">NsFloat32</span><span class="o">&gt;</span><span class="p">(</span><span class="n">DelayProperty</span><span class="p">);</span>
<span class="p">}</span>

<span class="c1">////////////////////////////////////////////////////////////////////////////////////////////////////</span>
<span class="kt">void</span> <span class="n">DelayedButton</span><span class="o">:</span><span class="n">SetDelay</span><span class="p">(</span><span class="n">NsFloat32</span> <span class="n">delay</span><span class="p">)</span>
<span class="p">{</span>
    <span class="n">SetValue</span><span class="o">&lt;</span><span class="n">NsFloat32</span><span class="o">&gt;</span><span class="p">(</span><span class="n">DelayProperty</span><span class="p">,</span> <span class="n">delay</span><span class="p">);</span>
<span class="p">}</span>

<span class="c1">// in use...</span>
<span class="n">Ptr</span><span class="o">&lt;</span><span class="n">DelayedButton</span><span class="o">&gt;</span> <span class="n">btn</span> <span class="o">=</span> <span class="n">NsCreateComponent</span><span class="o">&lt;</span><span class="n">DelayedButton</span><span class="o">&gt;</span><span class="p">();</span>
<span class="n">btn</span><span class="o">-&gt;</span><span class="n">SetDelay</span><span class="p">(</span><span class="mf">1.5f</span><span class="p">);</span>
</pre></div>
<p>As we see in the example, dependency properties can have extra information associated with a reflection type. This metadata is stored in a <a class="reference external" href="Gui.DependencySystem.PropertyMetadata.html">PropertyMetaData</a> object.</p>
</div>
<div class="section" id="wpf-compatibility">
<h1>WPF Compatibility</h1>
<p><a class="reference external" href="http://msdn.microsoft.com/en-us/library/system.windows.dependencyproperty.aspx">http://msdn.microsoft.com/en-us/library/system.windows.dependencyproperty.aspx</a></p>
<div class="line-block">
<div class="line"><br /></div>
<div class="line"><strong>Methods:</strong></div>
</div>
<table border="1" class="docutils">
<colgroup>
<col width="33%" />
<col width="2%" />
<col width="64%" />
</colgroup>
<thead valign="bottom">
<tr><th class="head">Name</th>
<th class="head">Sup</th>
<th class="head">Comments</th>
</tr>
</thead>
<tbody valign="top">
<tr><td>AddOwner</td>
<td>Yes</td>
<td>Implemented as DependencyData::AddOwner</td>
</tr>
<tr><td>GetMetadata</td>
<td>Yes</td>
<td>&nbsp;</td>
</tr>
<tr><td>IsValidType</td>
<td>No</td>
<td>&nbsp;</td>
</tr>
<tr><td>OverrideMetadata</td>
<td>Yes</td>
<td>Implemented as DependencyData::OverrideMetadata</td>
</tr>
<tr><td>Register</td>
<td>Yes</td>
<td>Implemented as DependencyData::RegisterProperty</td>
</tr>
<tr><td>RegisterAttached</td>
<td>No</td>
<td>Current implementation treats attached properties as normal dependency properties</td>
</tr>
<tr><td>RegisterAttachedReadOnly</td>
<td>No</td>
<td>Current implementation treats attached properties as normal dependency properties</td>
</tr>
<tr><td>RegisterReadOnly</td>
<td>Yes</td>
<td>Implemented as DependencyData::RegisterPropertyRO</td>
</tr>
</tbody>
</table>
<div class="line-block">
<div class="line"><br /></div>
<div class="line"><strong>Properties:</strong></div>
</div>
<table border="1" class="docutils">
<colgroup>
<col width="77%" />
<col width="6%" />
<col width="17%" />
</colgroup>
<thead valign="bottom">
<tr><th class="head">Name</th>
<th class="head">Sup</th>
<th class="head">Comments</th>
</tr>
</thead>
<tbody valign="top">
<tr><td>DefaultMetadata</td>
<td>No</td>
<td>&nbsp;</td>
</tr>
<tr><td>GlobalIndex</td>
<td>No</td>
<td>&nbsp;</td>
</tr>
<tr><td>Name</td>
<td>Yes</td>
<td>&nbsp;</td>
</tr>
<tr><td>OwnerType</td>
<td>Yes</td>
<td>&nbsp;</td>
</tr>
<tr><td>PropertyType</td>
<td>Yes</td>
<td>&nbsp;</td>
</tr>
<tr><td>ReadOnly</td>
<td>Yes</td>
<td>&nbsp;</td>
</tr>
<tr><td>ValidateValueCallback</td>
<td>Yes</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>
</div>
<br/>
<div>
    <div class="headerbg">
        <div class="footertext" style="height:32px; background:#262626;">
            &nbsp;<br/>2013 (C) Noesis Technologies
        </div>
    </div>
</div>
</div>
</body>
</html>
