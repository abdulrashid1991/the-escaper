using UnityEngine;
using Noesis;

namespace NoesisGUISamples
{

public class Touch : MonoBehaviour
{
    private Grid root_;

    void Start()
    {
        root_ = GetComponent<NoesisGUIPanel>().GetRoot<Grid>();

        ListBox list = root_.FindName<ListBox>("list");
        list.SelectionChanged += this.SelectionChanged;

        root_.ManipulationStarting += this.ManipulationStarting;
        root_.ManipulationInertiaStarting += this.ManipulationInertiaStarting;
        root_.ManipulationDelta += this.ManipulationDelta;
    }

    void SelectionChanged(BaseComponent sender, SelectionChangedEventArgs e)
    {
        ListBox list = sender.As<ListBox>();

        Image image = list.GetSelectedItem().As<Image>();
        root_.FindName<Image>("canvas").SetSource(image.GetSource());
        e.handled = true;
    }

    void ManipulationStarting(BaseComponent sender, ManipulationStartingEventArgs e)
    {
        e.mode = ManipulationModes.All;
        e.manipulationContainer = root_.FindName<Visual>("root");
        e.handled = true;
    }

    void ManipulationInertiaStarting(BaseComponent sender, ManipulationInertiaStartingEventArgs e)
    {
        e.desiredTranslationDeceleration = 100.0f / (1000.0f * 1000.0f);
        e.desiredRotationDeceleration = 360.0f / (1000.0f * 1000.0f);
        e.desiredExpansionDeceleration = 300.0f / (1000.0f * 1000.0f);
        e.handled = true;
    }

    void ManipulationDelta(BaseComponent sender, ManipulationDeltaEventArgs e)
    {
        Image image = e.source.As<Image>();
        MatrixTransform transform = image.GetRenderTransform().As<MatrixTransform>();
        Transform2f matrix = new Transform2f(transform.GetMatrix());

        float rotation = e.deltaManipulation.rotation * Mathf.Deg2Rad;
        float originX = e.manipulationOrigin.x;
        float originY = e.manipulationOrigin.y;
        float scale = e.deltaManipulation.scale;
        float translationX = e.deltaManipulation.translation.x;
        float translationY = e.deltaManipulation.translation.y;

        matrix.RotateAt(rotation, originX, originY);
        matrix.ScaleAt(scale, scale, originX, originY);
        matrix.Translate(translationX, translationY);

        transform.SetMatrix(matrix);
        e.handled = true;
    }
}

}
