using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;


namespace Noesis
{
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    [Noesis.Extended]
    public class DelegateCommand : Noesis.BaseCommand
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        private readonly Action action;
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        public DelegateCommand(Action action)
        {
            this.action = action;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        public bool CanExecuteCommand(BaseComponent parameter)
        {
            return true;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        public void ExecuteCommand(BaseComponent parameter)
        {
            action();
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////
    [Noesis.Extended]
    public class ViewModel : SerializableComponent
    {
        ////////////////////////////////////////////////////////////////////////////////////////////////////
        string _input = string.Empty;
        public string Input 
        { 
            get
            {
                return _input;
            }
            set
            {
                _input = value; 
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        private string _output = string.Empty;
        public string Output
        {
            get
            {
                return _output;
            }
            set
            {
                if (_output != value)
                {
                    _output = value;
                    NotifyPropertyChanged("Output");
                }
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        public DelegateCommand SayHelloCommand 
        { 
            get;
            private set;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        public ViewModel()
        {
            SayHelloCommand = new DelegateCommand(SayHello);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        private void SayHello()
        {
            Output = "Hello, " + Input;
        }
    }
}
